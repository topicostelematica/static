var getDataQuery = `
{
  materias{
    id,
    title,
    notas {
      id,
      valor,
      porcentaje,
      desc
    }
  }
}`

function getData () {
  $("#materias-container").html("");
  var payload = {
    query: getDataQuery
  }

  $.ajax({
    url: API_URL,
    type: 'POST',
    headers: {'Authorization': 'Bearer ' + getToken()},
    data: payload
  }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
        } else {
          renderData(data.data);
        }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })
}


function renderData(data){
  console.warn(data)
  var materias = data.materias;
  materias.reverse()
  for(var i = 0; i < materias.length; i++){
    var element = "";

    var id = materias[i].id;
    var title = materias[i].title;
    var notas = materias[i].notas;
    notas.reverse()
    for(var j = 0; j < notas.length; j++){
      console.log(notas.length);
      var notasID = notas[j].id;
      var notasDesc = notas[j].desc;
      var notasValor = notas[j].valor;
      var notasPorcentaje  = notas[j].porcentaje;

      element += `<tr data-id="${notasID}" id="nota-${notasID}" data-materia="${id}">
            <td>${notasDesc}</td>
            <td>${notasValor}</td>
            <td>${notasPorcentaje}</td>
            <td>
              <button class="del-nota ui compact icon negative button" data-id="${notasID}">
                <i class="trash icon"></i>
              </button>
              <button class="edit-nota ui compact icon primary button" data-id="${notasID}">
                <i class="write icon"></i>
              </button>
        </tr>`
    }

   element = `<div class="row" id="materia-${id}">
                          <table class="ui single line celled compact selectable table">
                              <thead>
                                  <tr>
                                      <th>${title}</th>
                                      <th>Nota</th>
                                      <th>%</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                ${element}
                              </tbody>
                              <tfoot class="full-width">
                                <tr>
                                  <td style="text-align: right">Acomulado:</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                  <tr>
                                      <th colspan="4">
                                          <div class="add-nota ui right floated small primary labeled icon compact button" data-id="${id}">
                                            <i class="plus icon"></i> Nota
                                          </div>
                                          <div class="del-materia ui small labeled icon compact button" data-id="${id}">
                                              <i class="trash icon"></i> Borrar materia
                                          </div>
                                      </th>
                                  </tr>
                              </tfoot>
                          </table>
                  </div>`
      $("#materias-container").append(element)
  }
  calculate();
}




function delMateria (id) {
  var materiaId = id;
  var payload = {
    query: `
      mutation DeleteMateria{
        deleteMateria(id: ${materiaId}) {
          id
        }
      }`
  }

  $.ajax({
    url: API_URL,
    dataType: 'json',
    contentType:"application/json; charset=utf-8",
    type: 'POST',
    headers: {'Authorization': 'Bearer ' + getToken()},
    data: JSON.stringify(payload)
  }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {
        $("#materia-"+id).remove();
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })
}



$(document).ready(function () {
  $("body").on( "click", ".del-materia", function(event) {
    event.preventDefault()

    var id = $(this).attr('data-id');
    $('#del-mat-modal')
      .modal({
        closable  : false,
        onDeny    : function(){
          return true;
        },
        onApprove : function() {
          delMateria(id);
        }
      })
      .modal('show');
  })

  $("body").on('click', '.add-nota', function (event) {
    event.preventDefault()


    var materiaId = $(this).attr('data-id');
    var inputs = `<tr>
                      <th>
                        <div class="ui mini input">
                          <input placeholder="Nombre" type="text">
                        </div>
                      </th>
                      <th>
                        <div class="ui mini input">
                          <input placeholder="Nota" type="text" size="2">
                        </div>
                      </th>
                      <th>
                        <div class="ui right labeled mini input">
                          <input placeholder="Porcentaje" type="text" size="2">
                          <div class="ui label">%</div>
                        </div>
                      </th>
                      <th>
                        <button class="save-add ui compact icon positive button" materia-id="${materiaId}">
                          <i class="save icon"></i>
                        </button>
                        <button class="cancel-add ui compact icon negative button">
                          <i class="remove icon"></i>
                        </button>
                      </th>
                  </tr>`;
    $(this).parent().parent().parent().prev().append(inputs);
    $(this).parent().parent().parent().prev().find('input[type=text],textarea,select').filter(':visible:first').focus();
  })


  $("body").on('click', '.save-add', function (event) {
    event.preventDefault();

    var self = this;
    var materiaId = $(this).attr('materia-id');

    var desc = "";
    var valor = "";
    var porcentaje = "";

    $(this).parent().parent().find('input[type=text],textarea,select').filter(':visible').each(function( index ) {
      switch(index){
        case 0:
          desc = $(this).val();
        break;
        case 1:
          valor = $(this).val();
        break;
        case 2:
          porcentaje = $(this).val();
        break;
        default:
      }
    });


    var payload = {
      query: `
        mutation CreateNota{
          createNota(desc: "${desc}", valor: ${valor}, porcentaje: ${porcentaje}, materia_id: ${materiaId}) {
            id,
            desc,
            valor,
            porcentaje,
            materia {
              id
            }
          }
        }`
    }

    $.ajax({
      url: API_URL,
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      type: 'POST',
      headers: {'Authorization': 'Bearer ' + getToken()},
      data: JSON.stringify(payload)
    }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {
        console.warn(data.data.createNota.id);
        //{"data":{"createNota":{"id":"6"}}}
        //$("#materia-"+id).remove();
        var element = `<tr data-id="${data.data.createNota.id}" id="nota-${data.data.createNota.id}" data-materia="${data.data.createNota.materia.id}">
            <td>${data.data.createNota.desc}</td>
            <td>${data.data.createNota.valor}</td>
            <td>${data.data.createNota.porcentaje}</td>
            <td>
              <button class="del-nota ui compact icon negative button" data-id="${data.data.createNota.id}">
                <i class="trash icon"></i>
              </button>
              <button class="edit-nota ui compact icon primary button" data-id="${data.data.createNota.id}">
                <i class="write icon"></i>
              </button>
        </tr>`
        $(self).parent().parent().parent().append(element)
        $(self).parent().parent().remove()

        calculate();
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })
  })

  $("body").on('click', '.cancel-add', function (event) {
    event.preventDefault();
    $(this).parent().parent().remove();
  })


  $("body").on('click', '.del-nota', function (event) {
    event.preventDefault();

    var notaId = $(this).attr('data-id');
    var payload = {
      query: `
        mutation DeleteNota{
          deleteNota(id: ${notaId}) {
            id
          }
        }`
    }

    $.ajax({
      url: API_URL,
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      type: 'POST',
      headers: {'Authorization': 'Bearer ' + getToken()},
      data: JSON.stringify(payload)
    }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {
        $("#nota-"+notaId).remove();
        calculate();
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })
  });



  $("body").on('click', '.edit-nota', function (event) {
    event.preventDefault();

    var self = this;

    var tr = $(self).parent().parent() //[<tr data-id=​"19" id=​"nota-19">​…​</tr>​]

    var desc = "";
    var valor = "";
    var porcentaje = "";

    console.log(tr)
    test = tr;

    $(tr).find('td').filter(':visible').each(function( index ) {
      switch(index){
        case 0:
          desc = $(this).text();
        break;
        case 1:
          valor = $(this).text();
        break;
        case 2:
          porcentaje = $(this).text();
        break;
        default:
      }
    });


    var notaId = $(this).attr('data-id');
    var materiaId = $(tr).attr('data-materia');
    var inputs = `<tr id="editing-${notaId}" data-materia="${materiaId}">
                      <th>
                        <div class="ui mini input">
                          <input placeholder="Nombre" type="text" value="${desc}">
                        </div>
                      </th>
                      <th>
                        <div class="ui mini input">
                          <input placeholder="Nota" type="text" size="2" value="${valor}">
                        </div>
                      </th>
                      <th>
                        <div class="ui right labeled mini input">
                          <input placeholder="Porcentaje" type="text" size="2" value="${porcentaje}">
                          <div class="ui label">%</div>
                        </div>
                      </th>
                      <th>
                        <button class="save-edit ui compact icon positive button" data-id="${notaId}" data-materia="${materiaId}">
                          <i class="save icon"></i>
                        </button>
                      </th>
                  </tr>`;
    $(tr).replaceWith(inputs);
  })


  $("body").on('click', '.save-edit', function (event) {


    event.preventDefault();

    var self = this;
    var notaId = $(this).attr('data-id');
    var materiaId = $(this).attr('data-materia');

    var desc = "";
    var valor = "";
    var porcentaje = "";

    $(this).parent().parent().find('input[type=text],textarea,select').filter(':visible').each(function( index ) {
      switch(index){
        case 0:
          desc = $(this).val()+"";
        break;
        case 1:
          valor = $(this).val();
        break;
        case 2:
          porcentaje = $(this).val();
        break;
        default:
      }
    });


    var payload = {
      query: `mutation UpdateNota {
                updateNota(id: ${notaId}, nota: {desc: "${desc}", valor: ${valor}, porcentaje: ${porcentaje}, materiaId: ${materiaId}}){
                  id,
                  desc,
                  valor,
                  porcentaje,
                  materia {
                    id
                  }
                }
              }`
    }

    $.ajax({
      url: API_URL,
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      type: 'POST',
      headers: {'Authorization': 'Bearer ' + getToken()},
      data: JSON.stringify(payload)
    }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {

        var element = `<tr data-id="${data.data.updateNota.id}" id="nota-${data.data.updateNota.id}" data-materia="${data.data.updateNota.materia.id}">
            <td>${data.data.updateNota.desc}</td>
            <td>${data.data.updateNota.valor}</td>
            <td>${data.data.updateNota.porcentaje}</td>
            <td>
              <button class="del-nota ui compact icon negative button" data-id="${data.data.updateNota.id}">
                <i class="trash icon"></i>
              </button>
              <button class="edit-nota ui compact icon primary button" data-id="${data.data.updateNota.id}">
                <i class="write icon"></i>
              </button>
        </tr>`
        $("#editing-"+data.data.updateNota.id).replaceWith(element);
        calculate();
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })


  })







  $("body").on('click', '#add-materia', function (event) {

    var name = $("#add-materia-name").val()

    var payload = {
      query: `
        mutation CreateMateria{
          createMateria(title: "${name}", userId: ${getID()}) {
            id
          }
        }`
    }

    $.ajax({
      url: API_URL,
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      type: 'POST',
      headers: {'Authorization': 'Bearer ' + getToken()},
      data: JSON.stringify(payload)
    }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {
        getData();
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })

  })





})


