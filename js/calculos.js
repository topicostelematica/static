function calculate(){

    $("body").find("tbody").each(function( index_tbody ) {
        var notas = [];
        var porcentajes = [];
        var porcentaje_restante = 100;

        $(this).find("tr").each(function( index_tr ) {
            var desc;
            var valor;
            var porcentaje;

            $(this).find("td").each(function( index_td ) {
                console.log(this);
                switch(index_td){
                    case 0:
                        desc = $(this).text();
                    break;
                    case 1:
                        valor = $(this).text();
                        if(valor < 3){
                            $(this).removeClass().addClass('negative');
                        }else if(valor <= 5){
                            $(this).removeClass().addClass('positive');
                        }
                    break;
                    case 2:
                        porcentaje = $(this).text();
                    break;
                    default:
                }
            })

            notas.push(valor);
            porcentajes.push(porcentaje);
            porcentaje_restante -= porcentaje;

        })

        console.log(notas);
        console.log(porcentajes);
        console.log(porcentaje_restante);
        console.log(getEstado(notas, porcentajes, porcentaje_restante));

        var estados = getEstado(notas, porcentajes, porcentaje_restante);
        var cuantoNecesita = estados[0];
        var status = estados[1];

        var notaAcomulada = 0;
        for(var i = 0; i < notas.length; i++){
            notaAcomulada += notas[i] * (porcentajes[i] / 100);
        }
        console.warn(notaAcomulada);

        $(this).parent().find("tfoot td").each(function( index_td ) {
            console.log(this);
            if(index_td == 1){
                $(this).html("<b>"+notaAcomulada.toFixed(2)+"</b>");
                if(notaAcomulada < 3){
                    $(this).removeClass().addClass('negative');
                }else{
                    $(this).removeClass().addClass('positive');
                }
            }else if(index_td == 2){
                $(this).text(100-porcentaje_restante);
            }else if(index_td == 3){
                $(this).text(status);
                switch(status){
                    case 0:
                        if(notaAcomulada < 3){
                            $(this).text("¡La has perdido! :(");
                            $(this).removeClass().addClass('negative');
                        }else{
                            $(this).text("¡Ya la has ganado! :D");
                            $(this).removeClass().addClass('positive');
                        }
                    break;
                    case 2:
                        $(this).text("¡Ya la has ganado! :(");
                        $(this).removeClass().addClass('positive');
                    break;
                    case 3:
                        $(this).html("¡Vas bien! Debes sacar <b>"+cuantoNecesita+"</b> en el <b>"+porcentaje_restante+"%</b> restante");
                        $(this).removeClass().addClass('warning');
                    break;
                    case 4:
                        $(this).html("¡Aún puedes! Debes sacar <b>"+cuantoNecesita+"</b> en el <b>"+porcentaje_restante+"%</b> restante");
                        $(this).removeClass().addClass('warning');
                    break;
                    case 5:
                        $(this).html("Ya la has perdido :( Deberías sacar <b>"+cuantoNecesita+"</b> en el <b>"+porcentaje_restante+"%</b> restante");
                        $(this).removeClass().addClass('negative');
                    break;
                    default:
                }
            }
        })


    })

}


function getEstado(notas, porcentajes, porcentaje_restante){
    var notaFinal = 0;
    var status = 0;

    var nota_deseada = 3;
    var nota_min = 0;
    var nota_max = 5;
    var nota_base = 3;

    if(porcentaje_restante < 0){
		 //display_error();
    }else if (porcentaje_restante == 0)
	{
		var valor_notas = 0;
		var estado;

		for(var i = 0; i < notas.length; i++)
		{
			valor_notas += ( notas[i] * (porcentajes[i] / 100) );
		}
		valor_nota_final = Math.round( valor_notas * 100 ) / 100;
		notaFinal = valor_nota_final;
        status =0;
	}
	else
	{
		var valor_notas = 0;
		var estado;

		for(var i = 0; i < notas.length; i++)
		{
			valor_notas += ( notas[i] * (porcentajes[i] / 100) );
		}

		valor_nota_final = ( nota_deseada - valor_notas ) / ( porcentaje_restante / 100 );
		valor_nota_final = Math.round( valor_nota_final * 100 ) / 100;


		if ( valor_nota_final > nota_max )
		{
			estado = 5;
		}
		else if ( valor_nota_final > nota_base && valor_nota_final <= nota_max )
		{
			estado = 4;
		}
		else if (valor_nota_final > nota_min && valor_nota_final <= nota_base)
		{
			estado = 3;
		}
		else if (valor_nota_final <= nota_min)
		{
			estado = 2;
			valor_nota_final = null;
		}
		porcentaje_restante = Math.round( porcentaje_restante * 100 ) / 100;

        notaFinal = valor_nota_final;
        status = estado;
		//show_modal(estado, valor_nota_final, porcentaje_restante);

	}

    return [notaFinal, status];
}

