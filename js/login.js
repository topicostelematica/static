/* LOGIN */
function saveToken (token) {
  localStorage.setItem('token', token)
}

function getToken () {
  return localStorage.getItem('token')
}

function saveID (id) {
  localStorage.setItem('userID', id)
}

function getID () {
  return localStorage.getItem('userID')
}

function logOut(){
  localStorage.removeItem('token');
  localStorage.removeItem('userID');
  $('#login-view').show()
  $('#app-view').hide()
  $('#login-email').val('')
  $('#login-password').val('')
}

function processLogin () {
  $('#login-view').hide()
  $('#app-view').show()
  getData();
}

var loginQuery = `
mutation UserLogin($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    token
  }
}`

function login (email, password) {
  var payload = {
    query: loginQuery,
    variables: { 'email': email, 'password': password}
  }

  $.post(API_URL, payload, function (response) {
    console.log(response)
    if (response.errors) {
      $('#login-form .error').html('<ul class="list"><li>El correo o contraseña no coinciden</li></ul>')
      $('#login-form .error').show()
    } else {
      if (response.data.login.token) {
        saveToken(response.data.login.token)
        profile();
        processLogin()
      }
    }
  })
}



function profile () {


    var payload = {
      query: `{
                user {
                  id
                }
              }`
    }

    $.ajax({
      url: API_URL,
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      type: 'POST',
      headers: {'Authorization': 'Bearer ' + getToken()},
      data: JSON.stringify(payload)
    }).done(function (data) {
      if (data.errors) {
          var guessError = data.errors[0].message;
          if(guessError.test(new RegExp("Not Authorized"))){
            logOut();
          }
      } else {
        if (data.data.user.id) {
          saveID(data.data.user.id)
        }
      }
    }).fail(function (jqXHR, textStatus) {
      alert('error: ' + textStatus)
    })


}

var joinQuery = `
mutation UserRegister($email: String!, $password: String!) {
  register(email: $email, password: $password) {
    id
  }
}`

function join (email, password) {
  var payload = {
    query: joinQuery,
    variables: { 'email': email, 'password': password}
  }

  $.post(API_URL, payload, function (response) {
    console.log(response)
    if (response.errors) {
      $('#login-form .error').html('<ul class="list"><li>El usuario ya existe o ha ocurrido un error.</li></ul>')
      $('#login-form .error').show()
    } else {
      if (response.data.register.id) {
        $('#login-action').click()
      }
    }
  })
}

$(document).ready(function () {
  $('#login-action').on('click', function (event) {
    event.preventDefault()

    $('#login-form').form('submit')

    if ($('#login-form').form('is valid')) {
      var email = $('#login-email').val()
      var pass = $('#login-password').val()
      login(email, pass)
    }
  })

  $('#join-action').on('click', function (event) {
    event.preventDefault()
    $('#login-form').form('submit')

    if ($('#login-form').form('is valid')) {
      var email = $('#login-email').val()
      var pass = $('#login-password').val()
      join(email, pass)
    }
  })
  $('#login-form').submit(function (event) {
    event.preventDefault()
  })

  $('#login-form').form({
    fields: {
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: 'Ingresa un usuario'
          }/*,
          {
            type: 'email',
            prompt: 'Ingresa un correo válido'
          }*/
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type: 'empty',
            prompt: 'Ingresa tu contraseña'
          },
          {
            type: 'length[6]',
            prompt: 'Tu contraseña debe tener al menos 6 caracteres'
          }
        ]
      }
    }
  })
})
